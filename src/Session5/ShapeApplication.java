package Session5;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by parlaghi on 10.08.2016.
 */
public class ShapeApplication {

    public static void main(String[] args) {

        List<Shape> shapes = new ArrayList<>();
        Circle circle = new Circle();
        shapes.add(circle);
        //Shape shape = new Shape();
        //shapes.add(shape);
        Rectangle rectangle = new Rectangle();
        shapes.add(rectangle);

        circle.setColor("green");
        //shape.setColor("orange");
        rectangle.setColor("blue");

        for (Shape s: shapes){
            System.out.println(s.getColor());
        }

        circle.setRadius(3);
        System.out.println("Perimeter: " + circle.getPerimeter());
        System.out.println("Area: " + circle.getArea());

        rectangle.setLength(2);
        rectangle.setWidth(3);
        System.out.println("Perimeter: " + rectangle.getPerimeter());
        System.out.println("Area: " + rectangle.getArea());
    }
}
