package Session5;

/**
 * Created by parlaghi on 10.08.2016.
 */
public class Circle extends Shape{

    private double radius;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getPerimeter(){

        return 2 * radius * Math.PI;
    }

    public double getArea(){
        return radius * radius * Math.PI;
    }
}
