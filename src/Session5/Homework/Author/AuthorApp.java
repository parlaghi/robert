package Session5.Homework.Author;

/**
 * Created by parlaghi on 16.08.2016.
 */
public class AuthorApp {

    public static void main(String[] args) {


                Author writer = new Author("Robert", "robert@gmail.com", 'm');
                //System.out.println(writer);

                Book book = new Book("Java for dummies", writer, 19.95, 99);
                System.out.println(book);

                book.setPrice(29.95);
                book.setQty(28);

                System.out.println(book.getAuthor());

                System.out.println("Title is " + book.getName());
                System.out.println("Price is "  + book.getPrice());
                System.out.println("Quantity is " + book.getQty());
                System.out.println("Author's name is " + book.getAuthor().getName());
                System.out.println("Author's email is " + book.getAuthor().getEmail());

                System.out.println(book.getAuthor().getName());
                System.out.println(book.getAuthor().getEmail());

                System.out.println("Author name method: " + book.getAuthorName());
                System.out.println("Author email method: " + book.getAuthorEmail());
                System.out.println("Author gender method: " + book.getAuthorGender());




            }
        }
