package Session5.Homework.MyDate;

/**
 * Created by parlaghi on 17.08.2016.
 */
public class MyDateApp {

    public static void main(String[] args) {

        MyDate date = new MyDate();

        System.out.println("Is a leap year: " + date.isLeapYear(1900));
        System.out.println("Is a valid date: " + date.isValidDate(1900, 11, 3));
        System.out.println("Days in month/year: " + date.daysInMonth(12, 2015));
        System.out.println("----------------------------------");

        date.setDate(1900, 2, 28);
        date.setYear(1666);
        date.setMonth(2);
        date.setDay(28);

    }
}
