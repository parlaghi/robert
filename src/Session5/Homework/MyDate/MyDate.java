package Session5.Homework.MyDate;

/**
 * Created by parlaghi on 17.08.2016.
 */
public class MyDate {

    private int year;
    private int month;
    private int day;

    private static String[] strMonths = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private static String[] strDays = {"Sunday", "Monday", "Tuesday", "wednesday", "Thursday", "Friday", "Saturday"};
    private static int[] daysInMonths = {31,28,31,30,31,30,31,31,30,31,30,31};

    public boolean isLeapYear(int year){

        return ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0)));
    }

    public int daysInMonth(int month, int year) {
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 31;
            case 2:
                if (isLeapYear(year))
                    return 29;
                else return 28;
            default:
                return 30;
        }
    }

    public  boolean isValidDate(int year, int month, int day){

        return  ((year >= 1 && year <= 9999) && (month >=1 && month <= 12)) && ((day >=1 && day <= daysInMonth(month, year)));
    }

    public void setDate(int year, int month, int day){
        if (isValidDate(year, month, day)){
            this.year = year;
            this.month = month;
            this.day = day;
        }
        else System.out.println("Invalid date");
    }

    public void setYear(int year){
        if (year >= 1 && year <= 9999){
            this.year = year;
        }
        else System.out.println("Invalid year");
    }

    public void setMonth(int month){
        if (month >= 1 && month <= 12){
            this.month = month;
        }
        else System.out.println("Invalid month");
    }

    public void setDay(int day){
        if (day >= 1 && day <= daysInMonth(month, year)){
            this.day = day;
        }
        else System.out.println("Invalid day");
    }

    public int getYear(){
        return year;
    }

    public int getMonth(){
        return month;
    }

    public int getDay(){
        return day;
    }

//    public String toString(int year, int month, int day){
//
//    }


}
