package Session5.Homework.MyTime;

/**
 * Created by parlaghi on 16.08.2016.
 */
public class NextTime {

    private MyTime time;

    public NextTime(MyTime time){
        this.time = time;
    }

    public MyTime getTime(){
        return this.time;
    }

    public MyTime nextSecond() {

        if (time.getSecond() != 59){
            time.setSecond(time.getSecond() + 1);
        }
        else {
            time.setSecond(0);
            if (time.getMinute() != 59) {
                time.setMinute(time.getMinute() + 1);
            } else {
                time.setMinute(0);
                if (time.getHour() != 23) {
                    time.setHour(time.getHour() + 1);
                } else
                    time.setHour(0);
            }
        }

        return time;
    }

    public MyTime nextMinute() {

        if (time.getMinute() != 59) {
            time.setMinute(time.getMinute() + 1);
        } else {
            time.setMinute(0);
            if (time.getHour() != 23) {
                time.setHour(time.getHour() + 1);
            } else time.setHour(0);
        }
        return time;
    }

    public MyTime nextHour(){

        if (time.getHour() != 23){
            time.setHour(time.getHour() + 1);
        }
        else time.setHour(0);
        return time;
    }

    public MyTime prevSecond(){

        if (time.getSecond() != 0) {
            time.setSecond(time.getSecond() - 1);
        }
        else {
            time.setSecond(59);
            if (time.getMinute() != 0){
                time.setMinute(time.getMinute() - 1);
            }
            else {
                time.setMinute(59);
                if (time.getHour() != 0){
                    time.setHour(time.getHour() - 1);
                }
                else
                    time.setHour(23);
            }
        }
        return time;
    }

    public MyTime prevMinute(){

        if (time.getMinute() != 0){
            time.setMinute(time.getMinute() - 1);
        }
        else {
            time.setMinute(59);
            if (time.getHour() != 0) {
                time.setHour(time.getHour() - 1);
            } else time.setHour(23);
        }
        return time;
    }

    public MyTime prevHour(){

        if (time.getHour() != 0){
            time.setHour(time.getHour() - 1);
        }
        else time.setHour(23);
        return time;
    }
}
