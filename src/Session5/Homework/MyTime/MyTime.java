package Session5.Homework.MyTime;

/**
 * Created by parlaghi on 16.08.2016.
 */
public class MyTime {

    private int hour;
    private int minute;
    private int second;

    public MyTime(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public void setTime(int hour, int minute, int second) {
        if ((hour >= 0 && hour < 24) && (minute >= 0 && minute < 60) && (second >= 0 && second < 60)) {
            this.hour = hour;
            this.minute = minute;
            this.second = second;
        } else System.out.println("Invalid hour, minute or second");
    }

    public void setHour(int hour) {
        if (hour >= 0 && hour < 24) {
            this.hour = hour;
        }
        else System.out.println("Invalid hour");
    }

    public void setMinute(int minute) {
        if (minute >= 0 && minute < 60) {
            this.minute = minute;
        }
        else System.out.println("Invalid minute");
    }

    public void setSecond(int second) {
        if (second >= 0 && second < 60) {
            this.second = second;
        }
        else System.out.println("Invalid second");
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSecond() {
        return second;
    }

    public String toString() {

        String second = Integer.toString(getSecond());
        String minute = Integer.toString(getMinute());
        String hour = Integer.toString(getHour());

        if (getSecond() < 10){
            second = "0" + getSecond();
        }

        if (getMinute() < 10){
            minute = "0" + getMinute();
        }

        if (getHour() < 10){
            hour = "0" + getHour();
        }
        return hour + ":" + minute + ":" + second;
    }
}