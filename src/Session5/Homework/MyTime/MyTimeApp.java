package Session5.Homework.MyTime;

/**
 * Created by parlaghi on 16.08.2016.
 */
public class MyTimeApp {

    public static void main(String[] args) {

        MyTime time = new MyTime(12, 1, 3);

        System.out.println("The time is: " + time);
        System.out.println("Get hour: " + time.getHour());
        System.out.println("Get minute: " + time.getMinute());
        System.out.println("Get second: " + time.getSecond());
        System.out.println("----------------------------------");

        time.setTime(14, 30, 0);
        System.out.println("The time is: " + time);
        System.out.println("Get hour: " + time.getHour());
        System.out.println("Get minute: " + time.getMinute());
        System.out.println("Get second: " + time.getSecond());
        System.out.println("----------------------------------");

        time.setHour(0);
        time.setMinute(1);
        time.setSecond(0);
        System.out.println("The time is: " + time);
        System.out.println("Get hour: " + time.getHour());
        System.out.println("Get minute: " + time.getMinute());
        System.out.println("Get second: " + time.getSecond());
        System.out.println("----------------------------------");

        NextTime nex = new NextTime(time);
//        System.out.println("The time is: " + nex.getTime());
//        System.out.println("Next second: " + nex.nextSecond());
//        System.out.println("----------------------------------");
//
//        System.out.println("Next minute: " + nex.nextMinute());
//        System.out.println("----------------------------------");
//
//        System.out.println("Next hour: " + nex.nextHour());
//        System.out.println("----------------------------------");


        System.out.println("Previous second: " + nex.prevSecond());
        System.out.println("----------------------------------");

        System.out.println("Previous minute: " + nex.prevMinute());
        System.out.println("----------------------------------");

        System.out.println("Previous hour: " + nex.prevHour());
        System.out.println("----------------------------------");

    }
}
