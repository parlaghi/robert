package Session5;

/**
 * Created by parlaghi on 10.08.2016.
 */
public class Square extends Rectangle{

    private double side;

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
}
