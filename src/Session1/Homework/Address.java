package Session1.Homework;

/**
 * Created by parlaghi on 10.08.2016.
 */
public class Address {

    private String city;

    public String getCity(){
        return city;
    }

    public void setCity(String city){
        this.city = city;
    }
}
