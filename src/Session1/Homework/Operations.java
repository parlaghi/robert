package Session1.Homework;

/**
 * Created by parlaghi on 14.07.2016.
 */
public class Operations {
    float number1;
    float number2;

    public void setNumber1(float number1){
        this.number1 = number1;
    }

    public void setNumber2(float number2) {
        this.number2 = number2;
    }

    public float getNumber1() {
        return number1;
    }

    public float getNumber2() {
        return number2;
    }

    public static float sum (float number1, float number2){
        return number1 + number2;
    }

    public static float substract (float number1, float number2){
        return number1 - number2;
    }

    public static float multiply (float number1, float number2){
        return number1 * number2;
    }

    public static float divide (float number1, float number2){
        return number1 / number2;
    }
}
