package Session1.Homework;


/**
 * Created by parlaghi on 12.07.2016.
 */
public class Person {

    String firstName;

    String lastName;

    String fullName;

    String eyesColour;

    private String cnp;

    int age;

    String greeting;

    String fullDescription;

    private Address address;

    public Person(String cnp){
        if (cnp.length() != 13){
            try {
                throw new ValidationException("cnp");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.cnp = cnp;
        System.out.println("Created person with CNP: " + cnp);
        address = new Address();
    };

    public void setCity(String city){
        this.address.setCity(city);
    }

    public String getCity(){
        return this.address.getCity();
    }

    public static final boolean HAS_HEART = true;

//    public void setHasHeart(boolean hasHeart){
//        this.hasHeart = hasHeart;
//    }

    public void setEyesColour(String eyesColour){
        this.eyesColour = eyesColour; //this = instanta curenta (e.g. "new")
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName (){
        return this.firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName (){
        return this.lastName;
    }

    public String getEyesColour() {
        return this.eyesColour;
    }

    public void setAge(int age) {
        if (age > 0) {
            this.age = age;
        }
        else {
            System.out.println("Invalid age");
        }
    }

    public int getAge(){
        return this.age;
    }

    public String printMyName (){
        fullName = firstName + " " + lastName;
        System.out.println(fullName);
        return this.fullName;
    }

    public String describeYourself (){
        //String fullDescription;

        if (age > 0) {
            fullDescription = "Full name: " + fullName + "\n" + "Age: " + Integer.toString(age); // alternative: printMyName instead fullName?
        }

        else System.out.println("invalid age!");

        System.out.println(fullDescription);

        return this.fullDescription;
    }

    public static int lastDigit(int age) {
        return Math.abs(age % 10);
    }

    public String birthday () {

        //String greeting;

        if (age > 0) {
            age++;

            if (lastDigit(age) == 1) {
                greeting = "Happy " + age + "st Birthday, " + firstName;
            }

            else if (lastDigit(age) == 2) {
                greeting = "Happy " + age + "nd Birthday, " + firstName;
            }

            else if (lastDigit(age) == 3) {
                greeting = "Happy " + age + "3d Birthday, " + firstName;
            }

            else {
                greeting = "Happy " + age + "th Birthday, " + firstName;
            }
        }

        else System.out.println("Invalid age!!");

       return this.greeting;
    }


}

