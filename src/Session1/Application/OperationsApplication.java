package Session1.Application;

import Session1.Homework.Operations;

/**
 * Created by parlaghi on 15.07.2016.
 */
public class OperationsApplication {
    public static void main(String[] args) {
        Operations op = new Operations();

        op.setNumber1(11.0f);
        op.setNumber2(65.8f);

        //float operator1 = op.getNumber1();
        //float operator2 = op.getNumber2();

        float operator1 = 11.0f;
        float operator2 = 65.8f;

        System.out.println("Sum: " + op.sum(operator1, operator2));
        System.out.println("Substract: " + op.substract(operator1, operator2));
        System.out.println("Multiply: " + op.multiply(operator1, operator2));
        System.out.println("Divide: " + op.divide(operator1, operator2));
    }
}
