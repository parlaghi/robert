package Session1.Application;

import Session1.Homework.Person;
import Session1.Homework.ValidationException;

/**
 * Created by parlaghi on 12.07.2016.
 */
public class PersonApplication {
    public static void main(String[] args) {
        Person me = null;
        try {
            me = new Person("189898");
            System.out.println(me);

            System.out.println(Person.HAS_HEART);

            me.setCity("Cluj");
            System.out.println(me.getCity());

            System.out.println(me.getEyesColour());
            me.setEyesColour("brown");
            System.out.println(me.getEyesColour());

            System.out.println(me.getAge());
            me.setAge(-5);
            System.out.println(me.getAge());


            System.out.println(me.getFirstName());
            me.setFirstName("Robert");
            me.setLastName("Parlaghi");
            me.printMyName();

            //System.out.println(me instanceof Person2);


            // System.out.println(me.getFirstName() + " " + me.getLastName());
            // System.out.println(me.printMyName());


            System.out.println(me.describeYourself());


            System.out.println(me.birthday());
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

}
