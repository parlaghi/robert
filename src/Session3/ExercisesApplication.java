package Session3;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by parlaghi on 02.08.2016.
 */
public class ExercisesApplication {

    public static void main(String[] args){


//        Calendar day = Calendar.getInstance();
//        int dayOfWeek = day.get(Calendar.DAY_OF_WEEK);
//
        boolean weekday = false; //(dayOfWeek >= Calendar.MONDAY) && (dayOfWeek <= Calendar.FRIDAY);

        boolean vacation = false;

        System.out.println("1.  we sleep in:  " + Exercises.getDate(weekday,vacation));

        System.out.println("2.  first three n times:  " + Exercises.frontTimes("Buha",4));

        System.out.println("3.  smiling monkeys:  " + Exercises.smiling(false, false));

        System.out.println("4.  every other char:  " + Exercises.everyOther("Heeololeo"));

        System.out.println("5.  sum or else:  " + Exercises.sumOrElse(5, 5));

        System.out.println("6.  return subtstring:  ??" );

        System.out.println("7.  absolute difference:  " + Exercises.absoluteDifference(10));

        int anArray[] = {1,9,9,3,9};
        System.out.println("8.  number of nines:  " + Exercises.arrayCount9(anArray));

        System.out.println("9.  talking parrot:  " + Exercises.talkingParrot(9, true));

        int is9[] = {1,2,3,4,9};
        System.out.println("10. any of first four is nine:  " + Exercises.arrayFront9(is9));

        System.out.println("11. check if numbers or their sum is ten:  " + Exercises.isTen(4,6));

        System.out.println("12. substring:  " + Exercises.altPairs("Chocolate"));

        System.out.println("13. negative:  " + Exercises.isNegative(-8,6,true));

        System.out.println("14. add strings together:  " + Exercises.addStrings("What","Up"));

        System.out.println("15. add 'not' if missing:  " + Exercises.addInFront("Al"));

        System.out.println("16. make tags:  " + Exercises.makeTags("cite", "Yay"));

        System.out.println("17. front three:  " + Exercises.front3(""));

        System.out.println("18. add word between 'out':  " + Exercises.wordBetweenOut("<<>>", "word"));

        System.out.println("19. add last char to front and end:  " + Exercises.backAround("word"));

        System.out.println("20. last 2 multiplied by 3:  " + Exercises.extraEnd("Hi"));

        System.out.println("21. mod 3 or 5:  " + Exercises.or35(10));

        System.out.println("22. first half: " + Exercises.firstHalf("HelloThere"));

        System.out.println("23. front two:  " + Exercises.front22("kitten"));

        System.out.println("24. remove first and last:  " + Exercises.withoutEnd("hellO"));

        System.out.println("25. starts with 'hi':  " + Exercises.startHi("hi there"));

        System.out.println("26. concatenate except first char:  " + Exercises.nonStart("shotl", "java"));

        System.out.println("27. ice or hot:  " + Exercises.icyHot(-120,2));

        int[] array = {13,6,1,2,3};
        System.out.println("28. first or last is six:  " + Exercises.firstLast6(array));

        System.out.println("29. n copies of string:  " + Exercises.stringTimes(".", 5));

        int[] firstLast = {7};
        System.out.println("30. same first and last:  " + Exercises.sameFirstLast(firstLast));

        int[] sum = {5,11,2};
        System.out.println("31. sum of elements:  " + Exercises.sum3(sum));

        int[] a = {4,3,5,3}; int[] b = {2,3};
        System.out.println("32. same first or last:  " + Exercises.sameEnd(a, b));

        int[] r = {1,2,3,4};
        System.out.println("34. reverse array:  " + Arrays.toString(Exercises.reverse3(r)));

        int[] toMax = {1,2,3};
        System.out.println("35. max end:  " + Arrays.toString(Exercises.maxEnd3(toMax)));

        int[] toSum = {1,2,3,4};
        System.out.println("36. sum of first 2:  " + Exercises.sum2(toSum));

        System.out.println("37. squirrels and cigars:  " + Exercises.cigarParty(50, false));

        System.out.println("38. date fashion:  " + Exercises.dateFashions(2,8));

        System.out.println("39. squirrels play:  " + Exercises.squirrelPlay(90, false));

        System.out.println("40. speeding:  " + Exercises.caughtSpeeding(65, true));

        System.out.println("41. sum of two ints..maybe:  " + Exercises.sortaSum(3, 4));

        System.out.println("42. is there a six:  " + Exercises.love6(-3, -9));

        System.out.println("43. outside mode on/off:  " + Exercises.in1To10(11, true));

        System.out.println("44. special number eleven:  " + Exercises.specialEleven(24));

        int[] avg = {19,1,10,2};
        System.out.println("45. centered average:  " + Exercises.centeredAverage(avg));

        int[] sum13 = {1,2,3,13,1,2,3,4};
        System.out.println("46=47. sum of elements except thirteen and next to thirteen:  " + Exercises.sum13(sum13));

        int[] sum67 = {1,2,2,6,99,99,7,1,2,3};
        System.out.println("48. sum of elements except 6->7:  " + Exercises.sum67(sum67));


    }
}
