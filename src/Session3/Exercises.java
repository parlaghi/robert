package Session3;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by parlaghi on 02.08.2016.
 */
public class Exercises {

    // Exercise 1: sleep in

    public static boolean getDate(boolean weekday, boolean vacation) {

        boolean sleepIn = (!weekday || vacation);

        return sleepIn;

    }

    // Exercise 2: front of the string

    public static String frontTimes(String str, int times) {

        String word = "";

        if (str.length() > 3) {

            for (int i = 0; i < times; i++) {
                word += str.substring(0, 3);
            }
        } else {
            for (int i = 0; i < times; i++) {
                word += str;

            }
        }
        return word;


//        char[] myChars = str.toCharArray();
//        String firstSome = "";
//
//        if (myChars.length >= 3) {
//
//            for (int i = 0; i < 3; i++) {
//                firstSome += myChars[i];
//            }
//
//            for (int j = 0; j < times; j++) {
//                System.out.print(firstSome);
//                ;
//            }
//
//        } else {
//            for (int k = 0; k < myChars.length; k++) {
//                firstSome += myChars[k];
//            }
//            for (int m = 0; m < times; m++)
//                System.out.print(firstSome);
//        }
//
//        return firstSome;
    }

    //Exercise 3: smiling monkeys

    public static boolean smiling(boolean aSmile, boolean bSmile) {

        boolean inTrouble = ((aSmile && bSmile) || (!aSmile) && (!bSmile));

        return inTrouble;
    }

    //Exercise 4: every other char from string

    public static String everyOther(String woord) {
        char[] myOdd = woord.toCharArray();
        String str = "";

        for (int i = 0; i < myOdd.length; i = i + 2) {
            str += myOdd[i];
        }

        return str;
    }

    //Exercise 5: sum or else..

    public static int sumOrElse(int a, int b) {
        int sum = a + b;
        int prod = a * b;

        if (a == b) {
            return prod;
        } else return sum;
    }

    //Exercise 6: return substring ??

    //Exercise 7: absolute difference

    public static int absoluteDifference(int n) {

        if (n > 21) {
            return Math.abs(n - 21);
        } else {
            return 2 * Math.abs(n - 21);
        }

    }

    //Exercise 8: number of 9s in array

    public static int arrayCount9(int myArray[]) {

        int numberOf9s = 0;

        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i] == 9) {
                numberOf9s += 1;
            }
        }

        return numberOf9s;
    }

    //Exercise 9: talking parrot

    public static boolean talkingParrot(int hour, boolean isTalking) {
        boolean inTrouble = ((hour < 7 || hour > 20) && isTalking);

//        SimpleDateFormat testHours = new SimpleDateFormat("hh:mm");
//        Date start = testHours.parse("07:00");
//        Date end = testHours.parse("20:00");
//        Date date = testHours.parse("10:00");
//
//        if ((date.getTime() < start.getTime() || (date.getTime() > end.getTime())) && isTalking) {
//            inTrouble = true;
//        }

        return inTrouble;
    }

    //Exercise 10: check if one of the first 4 elements is 9

    public static boolean arrayFront9(int myArray[]) {

        int nines = 0;
        boolean anyNines = false;

        if (myArray.length >= 4) {
            for (int i = 0; i <= 3; i++) {
                if (myArray[i] == 9) nines += 1;
            }
        } else {
            for (int i = 0; i < myArray.length; i++) {
                if (myArray[i] == 9) nines += 1;
            }
        }

        if (nines > 0) anyNines = true;

        return anyNines;
    }

    //Exercise 11: return true if one if them is 10 or if their sum is 10

    public static boolean isTen(int a, int b) {

        boolean checkTen = ((a == 10 || b == 10) || (a + b == 10));
        return checkTen;
    }

    //Exercise 12: return a string made of the chars at indexes .....


    public static String altPairs(String myString) {
        String mySubString = "";
        char[] myChar = myString.toCharArray();

        for (int i = 0; i < myChar.length; i += 4) {
            mySubString += myChar[i];
            if (i < myChar.length - 1)
                mySubString += myChar[i + 1];
        }
        return mySubString;

//        for (int i = 0; i < myChar.length; i += 4) {
//            mySubString += myChar[i];
//            if (i >= myChar.length - 1)
//                return mySubString;
//            else mySubString += myChar[i + 1];
//        }
//        return mySubString;
    }

    //Exercise 13: negative vs positive

    public static boolean isNegative(int a, int b, boolean negative) {
        boolean result = false;

        if (negative) {
            if ((a < 0) && (b < 0)) result = true;
        } else {
            if ((a > 0) && (b < 0) || (a < 0) && (b > 0)) result = true;
            else result = false;
        }

        return result;
    }

    //Exercise 14: adding string together

    public static String addStrings(String a, String b) {

        String result = a + b + b + a;
        return result;

    }

    //Exercise 15: adding "not" in front

    public static String addInFront(String myString) {
        String newString = "";

//        if(myString.substring(0,3).matches("not"))
//            newString = "not " + myString;
//        else return myString;

        if ((myString.length() >= 3) && (myString.substring(0, 3).equals("not")))
            return myString;
        else
            newString += "not " + myString;


//        if (!myString.startsWith("not"))
//            newString = "not " + myString;
//        else return myString;

        return newString;
    }

    //Exercise 16: make tags

    public static String makeTags(String tag, String word) {

        String resultString = "<" + tag + ">" + word + "</" + tag + ">";
        return resultString;
    }

    //Exercise 17: front 3

    public static String front3(String front) {
        String myString = "";

        if (front.length() < 3) {
            for (int i = 0; i < 3; i++)
                myString += front.substring(0, front.length());
        } else
            for (int i = 0; i < 3; i++) {
                    myString += front.substring(0, 3);
            }
        return myString;
    }

    //Exercise 18: word between "out"

    public static String wordBetweenOut(String out, String word) {
        String result = "";

        result += out.substring(0, 2) + word + out.substring(2, 4);

        return result;
    }

    //Exercise 19: add last char to beginning and end

    public static String backAround(String word) {
        String result = "";

        if (word.length() > 1) {
            for (int i = 0; i < word.length(); i++) {
                result = word.charAt(word.length() - 1) + word + word.charAt(word.length() - 1);
            }
        } else result = word.charAt(0) + word + word.charAt(0);

        return result;
    }

    //Exercise 20: last 2 chars x 3

    public static String extraEnd(String word) {
        String result = "";

        if (word.length() >= 2) {
            for (int i = 0; i < 3; i++) {
                result += word.substring(word.length() - 2, word.length());
            }
        } else return word;

        return result;
    }

    //Exercise 21: mod 3 or 5

    public static boolean or35(int a) {

        boolean result = (a % 3 == 0) || (a % 5 == 0);
        return result;
    }

    //Exercise 22: return first half of string

    public static String firstHalf(String word) {
        String result = "";

        if (word.length() % 2 == 0) {
            result += word.substring(0, word.length() / 2);
        } else return word;
        return result;
    }

    //Exercise 23: add first 2 chars to beginning and end

    public static String front22(String word) {
        String result = "";

        if (word.length() >= 2) {
            for (int i = 0; i < 2; i++) {
                String first2 = word.substring(0, 2);
                result = first2 + word + first2;
            }
        } else result = word.substring(0) + word + word.substring(0);
        return result;
    }

    //Exercise 24: return string without first and last char

    public static String withoutEnd(String word) {
        String result = "";

        if (word.length() >= 2) {
            result += word.substring(1, word.length() - 1);
        } else return word;
        return result;
    }

    //Exercise 25: starts with Hi

    public static boolean startHi(String word) {

        boolean result = (word.startsWith("hi"));
        return result;
    }

    //Exercise 26: concatenate, except first char of each

    public static String nonStart(String a, String b) {
        String result = "";

        if ((a.length() >= 1) && (b.length() >= 1)) {
            result = a.substring(1, a.length()) + b.substring(1, b.length());
        } else return result;
        return result;
    }

    //Exercise 27: ice or hot

    public static boolean icyHot(int a, int b) {

        boolean result = ((a < 0) && (b > 100)) || ((a > 100) && (b < 0));
        return result;
    }

    //Exercise 28: check if 6 is first or last

    public static boolean firstLast6(int[] numbers) {

        boolean result = (numbers.length >= 1) && ((numbers[0] == 6 || numbers[numbers.length - 1] == 6));
        return result;
    }

    //Exercise 29: n copies of string

    public static String stringTimes(String word, int n) {
        String result = "";

        for (int i = 0; i < n; i++) {
            result += word;
        }
        return result;
    }

    //Exercise 30: true if length > 1 and firs = last

    public static boolean sameFirstLast(int[] numbers) {

        boolean result = (numbers.length >= 1) && (numbers[0] == numbers[numbers.length - 1]);
        return result;
    }

    //Exercise 31: sum of all elements in array

    public static int sum3(int[] numbers) {
        int sum = 0;

        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }

        return sum;
    }

    //Exercise 32: same end

    public static boolean sameEnd(int[] a, int[] b) {

        boolean result = (a.length >= 1 && b.length >= 1) && ((a[0] == b[0]) || (a[a.length - 1] == b[b.length - 1]));
        return result;
    }

    //Exercise 33: rotate array

    //Exercise 34: reverse array

    public static int[] reverse3(int[] array) {
        int[] result = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            result[i] = array[array.length - 1 - i];
            //System.out.print(result[i]);
        }
        return result;
    }

    //Exercise 35: set all elements in array to largest value

    public static int[] maxEnd3(int[] array) {
        int[] result = new int[array.length];

        if (array.length > 0) {

            if (array[0] > array[array.length - 1]) {

                for (int i = 0; i < array.length; i++) {
                    result[i] = array[0];
                    //System.out.print(result[i]);
                }
            } else
                for (int i = 0; i < array.length; i++) {
                    result[i] = array[array.length - 1];
                    //System.out.print(result[i]);
                }
        }

        return result;
    }

    //Exercise 36: sum of first two in array

    public static int sum2(int[] array) {
        int result;

        if (array.length >= 2) {
            result = array[0] + array[1];
        } else if (array.length == 1) {
            return array[0];
        } else return 0;

        return result;
    }

    //Exercise 37: squirrels and cigars

    public static boolean cigarParty(int cigars, boolean isWeekend) {

        return (!isWeekend && cigars >= 40 && cigars <= 60) || (isWeekend && cigars >= 40);
    }

    //Exercise 38: date at restaurant

    public static int dateFashions(int you, int date) {
        int result;

        if (((you >= 8) || (date >= 8)) && ((you > 2) && (date > 2))) {
            result = 2;
        } else if ((you <= 2) || (date <= 2)) {
            result = 0;
        } else result = 1;

        return result;
    }

    //Exercise 39: squirrels playing

    public static boolean squirrelPlay(int temp, boolean isSummer) {

        return (!isSummer && (temp >= 60) && (temp <= 90)) || (isSummer && (temp >= 60) && (temp < 100));
    }

    //Exercise 40: caught speeding

    public static int caughtSpeeding(int speed, boolean isBirthday) {
        int result = 0;

        if ((!isBirthday && (speed <= 60)) || (isBirthday && (speed <= 65))) {
            result = 0;
        } else if ((!isBirthday) && (speed >= 61) && (speed <= 80) || ((isBirthday) && (speed > 65) && (speed <= 85))) {
            result = 1;
        } else if ((!isBirthday) && (speed >= 81) || ((isBirthday) && (speed > 85))) {
            result = 2;
        }

        return result;
    }

    //Exercise 41: sum of 2 except...

    public static int sortaSum(int a, int b) {
        int sum = 0;

        if ((a + b >= 10) && (a + b <= 20)) {
            sum = 20;
        } else sum = a + b;

        return sum;
    }

    //Exercise 42: the number 6

    public static boolean love6(int a, int b) {

        return (a == 6) || (b == 6) || (Math.abs(a + b) == 6) || (Math.abs(a - b) == 6);
    }

    //Exercise 43: outside mode ON vs OFF

    public static boolean in1To10(int n, boolean outsideMode) {

        return ((outsideMode) && ((n <= 1) || (n >= 10))) || (!outsideMode) && ((n >= 1) && (n <= 10));
    }

    //Exercise 44: special number 11

    public static boolean specialEleven(int n) {
        return (n % 11 == 0) || (n % 11 == 1);
    }

    //Exercise 45: centered average

    public static int centeredAverage(int[] array) {

        int min = 0;
        int max = 0;
        int sum = 0;

        if (array.length > 2) {

             min = Math.min(array[0], array[1]);
             max = Math.max(array[0], array[1]);
             sum = min + max;

            for (int i = 2; i < array.length; i++) {
                min = Math.min(min, array[i]);
                max = Math.max(max, array[i]);
                sum += array[i];
            }

            sum = sum - min - max;
        }
        else if (array.length == 2){
            return  (array[0]+array[1])/2;
        }
        else return array[0];


        int average = sum / (array.length - 2);
        return average;

    }

    //Exercise 46=47: sum of array, except 13 and next after 13

    public static int sum13(int[] array) {

        int sum = 0; int i = 0;

        while ((array.length > i) && (array[i] !=13)){
            sum += array[i];
            i++;
        }

        if (i < array.length){
            for (int j=i+2; j<array.length; j++){
                sum += array[j];
            }
        }

        return sum;
    }

    //Exercise 48: sum of array except '6' to '7'

    public static int sum67(int[] array){

        int i = 0;
        int sum = 0;

        while ((array.length > i) && (array[i] != 6)){
            sum += array[i];
            i++;
        }

        for (int j=i+1; j<array.length; j++) {
            if (array[j] == 7){
                for (int m=j+1; m<array.length; m++){
                    sum += array[m];
                }
            }
        }
        return sum;
    }

//    //Exercise 49: hashmap bully
//
//    public static Map<String, String> mapBully(Map<String, String> map1, Map<String, String> map2){
//
//
//    }
}