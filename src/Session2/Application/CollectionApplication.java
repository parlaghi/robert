package Session2.Application;

import Session2.Homework.CollectionUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by parlaghi on 25.07.2016.
 */
public class CollectionApplication {

    //static CollectionUtil cu = new CollectionUtil();

    public static void main(String[] args) {

        char[] mc = {'a','b','c','d'};
        CollectionUtil.reversed(mc);

        System.out.println("\n");

        List<Integer> list = new ArrayList<>();
        list.add(1); list.add(2); list.add(3); list.add(4);
        CollectionUtil.reverseManually(list);

        System.out.println("\n");

        List<Integer> collection = Arrays.asList(1,2,3,4,5,6,7,8);
        CollectionUtil.reverseWithCollections(collection);
    }


}
