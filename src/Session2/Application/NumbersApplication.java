package Session2.Application;


import Session2.Homework.NumbersUtil;

/**
 * Created by parlaghi on 25.07.2016.
 */
public class NumbersApplication {

    static NumbersUtil nu = new NumbersUtil();

        public static void main(String[] args) {

            nu.printOddNumbers(10);
            nu.isEvenNumber(7);
            nu.countOddNumbers(11);

            int m[][] = {
                {1,2,3},
                {4,5,6},
                {7,8,9}
            };

            nu.printMatrix(m);

        }
    }
