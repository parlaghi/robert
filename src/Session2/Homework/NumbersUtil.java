package Session2.Homework;

import java.util.ArrayList;

/**
 * Created by parlaghi on 25.07.2016.
 */
public class NumbersUtil {

    public static void printOddNumbers(int limit){

        ArrayList<Integer> oddNumbers = new ArrayList<Integer>();

       for (int i=0; i <= limit; i++){
           if (i%2 != 0){
               oddNumbers.add(i);
           }
       }
       System.out.println("The list of odd numbers: " + oddNumbers);
   }

    public static boolean isEvenNumber(int number){
        boolean j = false;
        if (number % 2 == 0) j = true;
        System.out.println("Your number is even = " + j);
        return j;
    }

    public static int countOddNumbers(int limit){
        int j = 0;
        for (int i = 0; i <= limit; i++){
            if (i%2 == 0) j++;
        }
        System.out.println("Number of odd numbers: " + j);
        return j;
    }

    public void printMatrix(int[][] matrix){

//        Scanner sc = new Scanner(System.in);
//
//        System.out.println("Enter number of rows: ");
//        int rows = sc.nextInt();
//
//        System.out.println("Enter number of columns: ");
//        int columns = sc.nextInt();
//
//        System.out.println("Enter the data :");
//
//        for(int i=0; i<rows; i++) {
//            for(int j=0; j<columns; j++) {
//                matrix[i][j] = sc.nextInt();
//            }
//        }

        for (int i=0; i<=matrix.length; i++){
            for (int j=0; j<matrix.length; j++){
              System.out.print(matrix[i][j]);
            }
            System.out.print("\n");
            }
        }
    }
