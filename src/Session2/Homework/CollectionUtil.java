package Session2.Homework;

import Session2.Application.CollectionApplication;

import java.util.*;

/**
 * Created by parlaghi on 25.07.2016.
 */
public class CollectionUtil {


    public static char[] reversed(char[] myChar){

        for (int i = myChar.length-1; i >= 0; i--){
            System.out.print(myChar[i] + " ");
        }

        return myChar;
    }

    public static List<Integer> reverseManually(List<Integer> myList){

        for (ListIterator<Integer> iterator = myList.listIterator(myList.size()); iterator.hasPrevious();){
            Integer prev = iterator.previous();
            System.out.print(prev + " ");
        }

        return myList;
    }

    public static List<Integer> reverseWithCollections(List<Integer> myCollection){

        Collections.reverse(myCollection);
        myCollection.forEach(System.out::print);

        return myCollection;
    }
}
