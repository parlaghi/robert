package Session6.Person2;

/**
 * Created by parlaghi on 18.08.2016.
 */
public class CollegeStudent extends Student {

    protected String major;
    protected int year;

    public CollegeStudent(String myName, int myAge, String myGender, String myIdNum, double myGPA, int myYear, String myMajor){
        super (myName, myAge, myGender, myIdNum, myGPA);
        major = myMajor;
        year = myYear;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String toString(){
        return super.toString() + ", year:" + year + ", major:" + major;
    }
}
