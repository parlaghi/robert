package Session6.Person2;

/**
 * Created by parlaghi on 18.08.2016.
 */
public class Student extends Person2 {
    protected String myIdNum;    // Student Id Number
    protected double myGPA;      // grade point average

    public Student(String name, int age, String gender, String idNum, double gpa)  {
// use the super class’ constructor
        super(name, age, gender);

// initialize what’s new to Student

        myIdNum = idNum;

        myGPA = gpa;

    }

    public String getMyIdNum() {
        return myIdNum;
    }

    public void setMyIdNum(String myIdNum) {
        this.myIdNum = myIdNum;
    }

    public double getMyGPA() {
        return myGPA;
    }

    public void setMyGPA(double myGPA) {
        this.myGPA = myGPA;
    }
}

