package Session6.Person2;

/**
 * Created by parlaghi on 18.08.2016.
 */
public class Person2 {

    protected String myName ;   // name of the person
    protected int myAge;        // person’s age
    protected String myGender;  // “M” for male, “F” for female

    public Person2(String name, int age, String gender)  {

        myName = name; myAge = age ; myGender = gender;   }

    public String toString()  {
        return myName + ", age: " + myAge + ", gender: " +myGender;
    }

    public String getMyName() {
        return myName;
    }

    public void setMyName(String myName) {
        this.myName = myName;
    }

    public int getMyAge() {
        return myAge;
    }

    public void setMyAge(int myAge) {
        this.myAge = myAge;
    }

    public String getMyGender() {
        return myGender;
    }

    public void setMyGender(String myGender) {
        this.myGender = myGender;
    }
}

