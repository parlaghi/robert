package Session6.Person2;

/**
 * Created by parlaghi on 18.08.2016.
 */
public class TestPerson {

    public static void main(String[] args) {

        Person2 bob = new Person2("Coach Bob", 27, "M");
        System.out.println(bob);

        Student lynne = new Student("Lynne Brooke", 16, "F", "HS95129", 3.5);
        System.out.println(lynne);

        Teacher mrJava = new Teacher("Duke Java", 34, "M", "Computer Science", 50000);
        System.out.println(mrJava);

        CollegeStudent ima = new CollegeStudent("Ima Frosh", 18, "F", "UCB123", 4.0, 1, "English");

        System.out.println(ima);


    }
}
