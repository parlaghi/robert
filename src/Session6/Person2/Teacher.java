package Session6.Person2;

import Session1.Homework.Person;

/**
 * Created by parlaghi on 18.08.2016.
 */
public class Teacher extends Person2 {

    protected String mySubject;
    protected double mySalary;

    public Teacher (String name, int age, String gender, String subject, double salary){
        super(name, age, gender);
        mySubject = subject;
        mySalary = salary;
    }

    public String getMySubject() {
        return mySubject;
    }

    public void setMySubject(String mySubject) {
        this.mySubject = mySubject;
    }

    public double getMySalary() {
        return mySalary;
    }

    public void setMySalary(double mySalary) {
        this.mySalary = mySalary;
    }

    public String toString(){
        return super.toString() + ", subject: " + mySubject + ", salary: " + mySalary;
    }
}
