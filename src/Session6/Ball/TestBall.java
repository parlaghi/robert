package Session6.Ball;

/**
 * Created by parlaghi on 18.08.2016.
 */
public class TestBall {

    public static void main(String[] args) {

        Ball ball = new Ball(2.0f, 3.0f, 4, 2, 3);
        System.out.println(ball);

        ball.setX(1.1f);
        ball.setY(2.1f);
        ball.setRadius(2);
        ball.setxDelta(3.2f);
        ball.setyDelta(4.1f);

        System.out.println(ball.getX());
        System.out.println(ball.getY());
        System.out.println(ball.getRadius());
        System.out.println(ball.getxDelta());
        System.out.println(ball.getyDelta());

    }
}
