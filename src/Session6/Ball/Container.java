package Session6.Ball;

/**
 * Created by parlaghi on 18.08.2016.
 */
public class Container {

    private int x1, y1, x2, y2;

    public Container(int x, int y, int width, int height){

        x1 = x;
        y1 = y;
        x2 = x1 + width - 1;
        y2 = y1 + width - 1;
    }

    public String toString(){

        return "Container at (" + x1 + ", " + y1 + ") to (" +  x2 +  ", " + y2 + ")";
    }
}
