package Session4;

/**
 * Created by parlaghi on 04.08.2016.
 */
public class ATMFactory {

    public static ATMInterface getATM(String bank) {

        if ("ing".equalsIgnoreCase(bank)) {
            return new IngATM();
        } else if ("brd".equalsIgnoreCase(bank)) {
            return new BrdATM();
        }
        return null;
    }
}
