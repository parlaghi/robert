package Session4;

import java.math.BigDecimal;

/**
 * Created by parlaghi on 04.08.2016.
 */
public interface ATMInterface {

    public BigDecimal retrieveBalance();

    public void withdrawMoney(BigDecimal amount);
}
