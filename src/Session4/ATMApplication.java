package Session4;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by parlaghi on 04.08.2016.
 */
public class ATMApplication {

    public static void main(String[] args) {

//        BigDecimal result = ATMFactory.getATM("brd").retrieveBalance();
//        System.out.println(result);
//        ATMFactory.getATM("ing").withdrawMoney(BigDecimal.ONE);

        ATMInterface brd = ATMFactory.getATM("brd");
        System.out.println(brd.retrieveBalance());
        brd.withdrawMoney(BigDecimal.ONE);

        ATMInterface ing = ATMFactory.getATM("inG");
        System.out.println(ing.retrieveBalance());
        ing.withdrawMoney(BigDecimal.ONE);

        List<ATMInterface> atms = new ArrayList<>();
        atms.add(brd);
        atms.add(ing);
        for (ATMInterface atm : atms){
            atm.withdrawMoney(new BigDecimal(123));
        }
    }
}
