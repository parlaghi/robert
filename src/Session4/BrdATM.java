package Session4;

import java.math.BigDecimal;

/**
 * Created by parlaghi on 04.08.2016.
 */
public class BrdATM implements ATMInterface {
    @Override
    public BigDecimal retrieveBalance() {
        System.out.println("retrieve balance BRD");
        return BigDecimal.ZERO;
    }

    @Override
    public void withdrawMoney(BigDecimal amount) {
        System.out.println("withdraw money BRD: " + amount);
    }
}
